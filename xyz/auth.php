<?php
include 'config.php';

    // ADD
    if(isset($_POST['username'])  AND isset($_POST['password'])){
        
        $user = $_POST['username'];
        $pass = $_POST['password'];
      
        $hashed_string['userName'] = $user;
        $hashed_string['userPassword'] = $pass;
        
        $data_post = array(
            'data' => $hashed_string,
        );
   
		$response = get_content_login($url.'session/login', json_encode($data_post));
        
        unset($_POST['username']);
        unset($_POST['password']);
        
        $response = json_decode($response);
        
		if(isset($response->status->error->message)){
            header('Location: ' ."login.php?error=".$response->status->error->message);
            exit;
		}else{
			$_SESSION['user']['UserName']   = $response->user->userName;
			$_SESSION['user']['FullName']   = $response->user->userFullName;
            $_SESSION['user']['Role']       = "Agent Rahasia";// $row['authRole'];
            $_SESSION['user']['Events']     = $response->events;
            if(isset($response->events[0])){
                $pilih = count($response->events)-1;
                
                if($pilih < 0){
                   $pilih = 0;
                }
                $_SESSION['user']['Event']          = $response->events[$pilih]->evnhId;
                $_SESSION['user']['EventName']      = $response->events[$pilih]->evnhName;
            }else{
                $_SESSION['user']['Event']      = 0;
                $_SESSION['user']['EventName']      = "N/A";
            }
            
            header("Location: dashboard");
            exit;
		}
		
    }

?>
