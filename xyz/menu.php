<?php



$_username  = $_SESSION['user']['UserName'];
$_fullname  = $_SESSION['user']['FullName'];
$_role      = $_SESSION['user']['Role'];


$response = get_content_login($titu.'api/v1/event/'.$_SESSION['user']['Event'], NULL);

$response = json_decode($response);

if($response->data->evnhType == 7){
    
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
  
		<li {{invite}}><a href='/xyz/invite'><i class='fa fa-hourglass-half'></i> <span>Invitation</span><span class='pull-right-container'></span></a></li>
        <li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        <!--    
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>

        <li {{import}}><a href='/xyz/import'><i class='fa fa-user-plus'></i> <span>Import</span><span class='pull-right-container'></span></a></li>


        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}else if($response->data->evnhType == 2){
    
    $menu = "
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    
    <aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-tachometer-alt'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        <li {{courier}}><a href='/xyz/courier'><i class='fa fa-truck'></i> <span>Courier</span><span class='pull-right-container'></span></a></li>
        
        <li {{runner}}><a href='/xyz/runner'><i class='fa fa-running'></i> <span>Runner</span><span class='pull-right-container'></span></a></li>
        <li {{runnerreport}}><a href='/xyz/runnerreport'><i class='fa fa-bar-chart'></i> <span>Runner Report</span><span class='pull-right-container'></span></a></li>
        
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        <!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket-alt'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>
        
        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}else if($response->data->evnhType == 99){
    
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
        <li {{report}}><a href='/xyz/report'><i class='fa fa-truck'></i> <span>Swab Test Results</span><span class='pull-right-container'></span></a></li>
       
  
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        
        <!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>

        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        
        
        </section>
        <!-- /.sidebar -->
    </aside>";
    
}
else{
    $menu = "<aside class='main-sidebar'>

    <section class='sidebar'>
      <!-- Sidebar user panel -->
      <div class='user-panel'>
        <div class='pull-left image'>
          <img src='/../dist/img/Karakter_titoe-02.png' class='img-circle' alt='User Image'>
        </div>
        <div class='pull-left info'>
          <p>$_fullname</p>
          <a href='#'><i class='fa fa-circle text-success'></i> $_role</a>
        </div>
      </div>
      <!-- search form -->
      <form action='#' method='get' class='sidebar-form'>
        <div class='input-group'>
          <input type='text' name='q' class='form-control' placeholder='Search...'>
          <span class='input-group-btn'>
                <button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class='sidebar-menu' data-widget='tree'>
        <li class='header' >MAIN NAVIGATION</li>
        <li {{dashboard}}><a href='/xyz/dashboard'><i class='fa fa-dashboard'></i> <span>Dashboard</span><span class='pull-right-container'></span></a></li>
        
        
  
		<li {{transaction}}><a href='/xyz/transaction'><i class='ion ion-ios-cart-outline'></i> <span>Transaction</span><span class='pull-right-container'></span></a></li>
        <li {{participants}}><a href='/xyz/participants'><i class='ion ion-ios-people-outline'></i> <span>Participants</span><span class='pull-right-container'></span></a></li>
		<li {{coupons}}><a href='/xyz/coupons'><i class='fa fa-tag'></i> <span>Coupons</span><span class='pull-right-container'></span></a></li>
        <!--
        <li {{couponstransaction}}><a href='/xyz/couponstransaction'><i class='fa fa-tags'></i> <span>Coupons Transaction</span><span class='pull-right-container'></span></a></li>
        -->
        <li {{ticket}}><a href='/xyz/ticket'><i class='fa fa-ticket'></i> <span>Ticket</span><span class='pull-right-container'></span></a></li>
        <li {{import}}><a href='/xyz/import'><i class='fa fa-user-plus'></i> <span>Import</span><span class='pull-right-container'></span></a></li>

        <!--
        <li class='treeview {{transaction}}'>
          <a href='#'>
            <i class='ion ion-ios-cart-outline'></i> <span>Transaction</span>
            <span class='pull-right-container'>
              <i class='fa fa-angle-left pull-right'></i>
            </span>
          </a>
          <ul class='treeview-menu'>
            <li {{transaction_registration}}><a href='/xyz/registration'><i class='ion ion-ios-people-outline'></i>Registration</a></li>
          </ul>
        </li>
        -->    

        
        </section>
        <!-- /.sidebar -->
    </aside>";
}

  
  








?>