<?php
require __DIR__ . '/config.php';
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


if(!isset($_GET['id']) OR !isset($_GET['name'])){
    header('Location: upload.php');
    exit;
}

$id   = $_GET['id'];
$filename = $_GET['name'];


$result = mysqli_query($conn, "SELECT fbniAccountNo, fbniPostDate, fbniValueDate, fbniBranch, fbniJournalNo, fbniDescription, fbniDebit, fbniCredit, fbniVA, fbniVACustomer, bnivOrderId, bnivNtb, bnivVANumber, bnivEvent FROM view_data WHERE fbniFlimId = $id");

$rows = array('AccountNo', 'Post Date', 'Value Date', 'Branch', 'Journal No', 'Description', 'Debit', 'Credit', 'VA', 'VA Customer', 'Reff Id', 'Ntb / Journal', 'VA Number', 'Event');

$conn->close();




header( "Content-Type: text/csv;charset=utf-8" );
header( "Content-Disposition: attachment;filename=\"$filename\"" );
header("Pragma: no-cache");
header("Expires: 0");
$fp = fopen('php://output', 'w');

fputcsv($fp, $rows);

while($r=mysqli_fetch_array($result))
{
    
    unset($r[0]);
    unset($r[1]);
    unset($r[2]);
    unset($r[3]);
    unset($r[4]);
    unset($r[5]);
    unset($r[6]);
    unset($r[7]);
    unset($r[8]);
    unset($r[9]);
    unset($r[10]);
    unset($r[11]);
    unset($r[12]);
    unset($r[13]);
    
    fputcsv($fp, $r);
}	


fclose($fp);




?>