<?php 
include __DIR__.'/../xyz/menu.php';


$EVENT = $_SESSION['user']['Event'];

$SEETLYTOEVA = "N/A";
$JUMLAH = "N/A";


$pageNumber = 1;
$pageSize = 10;


if(isset($_get['pageNumber'])){
    $pageNumber = $_get['pageNumber'];
}

if(isset($_get['pageSize'])){
    $pageSize = $_get['pageSize'];
}

if(isset($_get['search'])){
    $_get['search'] = urldecode($_get['search']);
    $cari = $_get['search'];
    $search = '&search='.$_get['search'];

}else{
    $search = "";
}


    // GET DATA
    $ch = curl_init(); 
    $url_x = $titu."api/v1/resources/event_category?filter[evncEvnhId]=$EVENT";
        

    // set url
    curl_setopt($ch, CURLOPT_URL, $url_x);
    // return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    // $output contains the output string 
    $output = curl_exec($ch); 
    // tutup curl 
    curl_close($ch);      
    // menampilkan hasil curl
    $data_all_caegory = json_decode($output); 
    
    
    

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Steelytoe Xyz</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  
    <!-- Select2 -->
  <link rel="stylesheet" href="../bower_components/select2/dist/css/select2.min.css">
    <!-- daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>Xyz</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Steelytoe</b>Xyz</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="hidden-xs"> <?php echo $_SESSION['user']['EventName']; ?> &nbsp; </span>
              <i class="fa fa-calendar"> </i>
              <span class="label label-success"><?php echo COUNT($_SESSION['user']['Events']); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo COUNT($_SESSION['user']['Events']); ?> events</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php
                    foreach($_SESSION['user']['Events'] AS $vall){
                        echo "<li><a href='".'../xyz/event/'.$vall->evnhId."'><h3>".$vall->evnhName."</i></h3></a></li>";
                    }
                  
                ?>
 
                </ul>
              </li>
              <li class="footer"><a href="#">Close</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="../xyz/logout.php" class="dropdown-toggle" >
             
               <span class="hidden-xs">Sign Out</span>
				<i class="fa fa-sign-out"> </i>
            </a>

              
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
    

    $menu = str_replace("{{coupons}}","class='active'",$menu);
    echo $menu;
  
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Coupons
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Coupons</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        
      
      
      
        <!-- left column -->
        <div class="col-md-12">
          

          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">DATA COUPONS</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                    <div class="col-sm-2">
                         <a onclick="window.open('<?php echo $titu."/api/v1/datacoupon/".$EVENT  ?>')" class="btn btn-block btn-social btn-bitbucket" > 
                        <i class="fa fa-save"></i> Save CSV</a>
                        
                        <!--
                        <div class="form-group">
                           
                            <select class="form-control select2" style="width: 100%;">
                              <option selected="selected">Alabama</option>
                              <option>Alaska</option>
                              <option disabled="disabled">California (disabled)</option>
                              <option>Delaware</option>
                              <option>Tennessee</option>
                              <option>Texas</option>
                              <option>Washington</option>
                            </select>
                        </div>
                        -->
                    </div>
                    
               
                    
                        <!--
                        <div class="dataTables_length" id="example2_length">
                            <label>Show <select name="example2_length" aria-controls="example2" class="form-control input-sm" disabled>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> entries</label>
                             
                        </div>
                        
                        -->
               
            
                    <div class="col-sm-10">
                        <div id="example2_filter" class="dataTables_filter">
                            <form>
                                <input name="pageNumber" type="hidden" value ="<?php echo $pageNumber;?>"> </input>
                                <label>Search By Name : <input value ="<?php if(isset($_get['search'])) echo $_get['search']; ?>" name="search" type="search" class="form-control input-sm" placeholder="" aria-controls="example2"></label>
                            </form>
                        </div>
                    </div>
                </div>
               
                
             



             <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
					
                    <th>Coupon Name</th>
                    <th>Code</th>
                    <th>Note</th>
                    <th>Description</th>
                    <th>Value</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Min</th>
                    <th>Max</th>
                    <th>Quota</th>
                    <th>Used</th>
                    <th>Discount</th>
                    
                </tr>
                

                </thead>
                <tbody>
                
                <?php
                    

                    // GET DATA
                    $ch = curl_init(); 
                    
                    // http://dev.titudev.com/api/v1/resources/transaction_coupon?filter[trcpEventId]=169
                    if(isset($cari)){
                        $url_ = $titu."api/v1/resources/coupon?pageNumber=$pageNumber&pageSize=$pageSize&filter[cupnEvnhId]=$EVENT&filter[cupnName][like]=%25".urlencode($cari)."%25";
                    }else{
                        $url_ = $titu."api/v1/resources/coupon?pageNumber=$pageNumber&pageSize=$pageSize&filter[cupnEvnhId]=$EVENT";
                    }
                    

                    // set url
                    curl_setopt($ch, CURLOPT_URL, $url_);

                    // return the transfer as a string 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                

                    // $output contains the output string 
                    $output = curl_exec($ch); 

                    // tutup curl 
                    curl_close($ch);      

                    // menampilkan hasil curl
                    $data_all = json_decode($output);
                    

					if(isset($data_all->linked->cuprCupnId)){
						foreach($data_all->linked->cuprCupnId as $v){
							$data_all->linked->cuprCupnId[$v->cuprCupnId] = $v;
							
						}
					}
      
                    if(isset($data_all->linked->trcpCupnId)){
						foreach($data_all->linked->trcpCupnId as $v){
							$data_all->linked->trcpCupnId[$v->trcpId] = $v;
						}
					}

                    if(isset($data_all->data)){
                        
                        	
  
                        foreach($data_all->data as $vall ){
                            $diskon = 0;
                            
                            foreach($vall->links->trcpCupnId AS $ds){
                              
                                $diskon = $diskon + $data_all->linked->trcpCupnId[$ds]->trcpDiscount;

                            }
                            
                            
                            echo "<tr>";
							
                            echo "<td>".$vall->cupnName."</td>";
							echo "<td>".$vall->cupnCode."</td>";
                            echo "<td>".$vall->cupnNotes."</td>";
                            echo "<td>".$vall->cupnDescription."</td>";

                  
                            
							if($vall->cupnType == 1){
								$vall->cupnValue = $vall->cupnValue."%";
							}else{
								$vall->cupnValue = "Rp. ".number_format($vall->cupnValue,2,",",".");
							}
							echo "<td>".$vall->cupnValue."</td>"; 
                            
                            echo "<td>".$vall->cupnStartTime."</td>"; 
                            echo "<td>".$vall->cupnEndTime."</td>"; 
                            echo "<td>".$data_all->linked->cuprCupnId[$vall->cupnId]->cuprMinPerson."</td>"; 
                            echo "<td>".$data_all->linked->cuprCupnId[$vall->cupnId]->cuprMaxPerson."</td>"; 
                            echo "<td>".$data_all->linked->cuprCupnId[$vall->cupnId]->cuprQuota."</td>"; 
                            
                            
                            echo "<td>".count($vall->links->trcpCupnId)."</td>"; 
                            echo "<td>".$vall->cupnValue = "Rp. ".number_format($diskon,2,",",".")."</td>"; 

							
                            /*

							
                            echo "<td>".$vall->trcpDiscount."</td>";               
                            
							
                            
							if($vall->trcpStatus == 1){
                                $vall->trcpStatus = '<span class="label label-success">Confirmed</span>';
                            }else{
                                $vall->trcpStatus = '<span class="label label-warning">Pending</span>';
                            }
                            
                            echo "<td>".$vall->trcpStatus."</td>";
                      
                            */
                            echo "</tr>";
                            
                        
                        }
                    
                    }

                ?>
                
                </tbody>
                <tfoot>
                
                <tr>
			
                    <th>Coupon Name</th>
                    <th>Code</th>
                    <th>Note</th>
                    <th>Description</th>
                    <th>Value</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Min</th>
                    <th>Max</th>
                    <th>Quota</th>
                    <th>Used</th>
                    <th>Discount</th>
                    
                  
                </tr>
                </tfoot>
              </table>
              
              <!-- INFO -->
              
              <?php
                $total = 0;
                if(isset($data_all->status->totalRecords)){
            
                    $total = $data_all->status->totalRecords;
                
                }
                
                $tmp = $total/10;
                $tmp = ceil($tmp);
                $tmp_ = 0;
                $i = $pageNumber - 2;
                if($i <= 0){
                    $i = 1; 
                }
                
                $max = $pageNumber * $pageSize;
                $min = $max - $pageSize;
                if($max > $total){
                    $max = $total;
                }
                if($min <= 0){
                    $min = 1;
                }
                
              ?>
              <div class="row">
                <div class="col-sm-5">
                   
                   <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing <?php echo  $min;?>  to <?php echo  $max;?> of <?php echo  $total;?> entries</div>

                </div>
              
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                        
                        <?php
                        
                            $next = $pageNumber+1;
                            $prev = $pageNumber-1;
                            
                            if($prev <=0){
                                echo "<li class='paginate_button previous disabled'><a>Previous</a></li>";
                            }else{
                                echo "<li class='paginate_button previous'><a href='?pageNumber=$prev$search'>Previous</a></li>";
                            }
                            
                            
  
                            if($i <> 1){
                                echo "<li class='paginate_button'><a href='?pageNumber=1$search'>1</a></li>";
                                echo "<li class='paginate_button'><a>..</a></li>";
                                
                            }
  
                            
                            
                            for($i; $i < $tmp; $i++){
                                if($tmp_ == 5){
                                    echo "<li class='paginate_button'><a>..</a></li>";
                                    break;
                                }
                                
   
                                if($pageNumber == $i){
                                    echo "<li class='paginate_button active'><a href='?pageNumber=$i$search'> $i</a></li>";
                                }else{
                                    echo "<li class='paginate_button'><a href='?pageNumber=$i$search'>$i</a></li>";
                                }
                                
                                $tmp_++;
                                
                            }
                            if($tmp == $pageNumber){
                                echo "<li class='paginate_button active'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }else{
                                echo "<li class='paginate_button'><a href='?pageNumber=$tmp$search'> $tmp</a></li>";
                            }
                            
                            
                            if($next >  $tmp){
                                echo "<li class='paginate_button next disabled' ><a>Next</a></li>"; 
                            }else{
                                echo "<li class='paginate_button next' ><a href='?pageNumber=$next$search'>Next</a></li>";
                            }
                            
                            
                        ?>

                            
                        </ul>
                    </div>
                </div>
              </div>
            
            
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>

      </div>
      <!-- /.row -->
      
      <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Quick Crate Coupon</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="/xyz/createcupon.php" method="post" class="form-horizontal" role="form">
                  <div class="box-body">
                    <div hidden class="form-group">
                        <label for="event" class="col-sm-3 control-label">Event</label>
                        <div class="col-sm-9">
                            <input name='data[cocrEvnhId]' required  value ="<?php echo $EVENT ?>" type="number" class="form-control" id="event" placeholder="Jumlah kupon dibuat">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="uniquenote" class="col-sm-3 control-label">Unique Note</label>
                        <div  class="col-sm-9">
                            <input readonly  name='data[cocrCouponUniqueNote]'  required type="text" value="<?php echo date("Y-md-Hi-s").substr(uniqid('', true), -2).'-'.strtoupper(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 4)); ?>" class="form-control" id="uniquenote" placeholder="Unik kupon yang dibuat">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="total" class="col-sm-3 control-label">Coupon Total</label>
                        <div class="col-sm-9">
                            <input required name='data[cocrCouponJumlah]' min="1" type="number" class="form-control" id="total" placeholder="Total kupon yang dibuat">
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="name" class="col-sm-3 control-label">Coupon Name</label>
                        <div class="col-sm-9">
                            <input name='data[cocrCouponName]' required type="text" class="form-control" id="name" placeholder="Nama kupon yang dibuat">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="prefix" class="col-sm-3 control-label">Coupon Prefix</label>
                        <div class="col-sm-9">
                            <input name='data[cocrCouponPrefix]' required minlength="3" maxlength="8" type="text" class="form-control" id="prefix" placeholder="Prefix kupon kode yang dibuat, Maksiaml 8 karakter">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="prefix" class="col-sm-3 control-label">Coupon Type</label>
                        <div class="col-sm-9">
                          <!--
                          <input name='data[cocrCouponType]' required minlength="3" maxlength="8" type="text" class="form-control" id="prefix" placeholder="Type kupon kode yang dibuat">
                          --> 
                            <select required name='data[cocrCouponType]' class="form-control">
                                <option value = "1">Percentage</option>
                                <option value = "2">Amount</option>

                            </select>
                        
                        
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="prefix" class="col-sm-3 control-label">Coupon Value</label>
                        <div class="col-sm-9">
                            <input name='data[cocrCouponValue]' required minlength="3" maxlength="8" type="number" class="form-control" id="prefix" placeholder="Nominal kupon kode yang dibuat">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Coupon Description</label>
                        <div class="col-sm-9">
                            <input name='data[cocrCouponDescription]' type="textarea" class="form-control" id="description" placeholder="Description kupon dibuat">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-3 control-label">Coupon Date</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            
                                <input name='data[cocrCouponStartEnd]' required type="text" class="form-control" id="reservationtime" placeholder="Description kupon dibuat">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="total" class="col-sm-3 control-label">Rule</label>
                        <div class="col-sm-9">
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="total" class="col-sm-3 control-label">Quota Rule</label>
                        <div class="col-sm-9">
                            <input name='data[cocrCouponQuota]' required min="1" type="number" class="form-control" id="quota" placeholder="Berapa kali kupon dapat digunakan">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="total" class="col-sm-3 control-label">Min And Max Rule</label>
                        <div class="col-sm-4">
                            <input name='data[cocrCouponMin]' required min="1" type="number" class="form-control" id="min" placeholder="Min">
                        </div>
                        <div class="col-sm-1">
                             <label for="total" class="col-sm-3 control-label">-</label>
                        </div>
                              <div class="col-sm-4">
                            <input name='data[cocrCouponMax]' required min="1" type="number" class="form-control" id="max" placeholder="Max">
                        </div>
                    </div>
                    
                    

                    
                    <?php
                    $label_category = "Category";
                    foreach($data_all_caegory->data AS $data){

                    echo '<div class="form-group">';   
                        echo '<label for="total"  class="col-sm-3 control-label">'.$label_category.'</label>';
                            echo '<div class="col-sm-9">';
                              echo '<div class="checkbox">';
                                echo '<label>';
                                  echo '<input name="data[cocrCouponCategory][]" id="uniquenote-'.$data->id.'" value ="'.$data->id.'" type="checkbox">';
                                  echo  $data->evncName;
                                echo '</label>';
                              echo '</div>';
                             echo '</div>';
                    echo '</div>';
                    $label_category = "";
                    }
                    ?>
                    
         
                    
                    

                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button   type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
        </div>
        
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Update Duration Coupon</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form">
                  <div class="box-body">
                    <div hidden class="form-group">
                        <label for="event" class="col-sm-3 control-label">Event</label>
                        <div class="col-sm-9">
                            <input required disabled value ="<?php echo $EVENT ?>" type="number" class="form-control" id="event" placeholder="Jumlah kupon dibuat">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="uniquenote" class="col-sm-3 control-label">Unique Note</label>
                        <div class="col-sm-9">
                            <input disabled required type="text" value="All" class="form-control" id="uniquenote" placeholder="Unik kupon yang dibuat">
                        </div>
                    </div>
                    
                    
                    
                    <div class="form-group">
                        <label for="total" class="col-sm-3 control-label">Min And Max Rule</label>
                        <div class="col-sm-4">
                            <input required  type="text" class="form-control" id="datepickermin" placeholder="Min">
                        </div>
                        <div class="col-sm-1">
                             <label for="total" class="col-sm-3 control-label">-</label>
                        </div>
                              <div class="col-sm-4">
                            <input required  type="text" class="form-control" id="datepickermax" placeholder="Max">
                        </div>
                    </div>
                    
                    

                    
                   

                  </div>
                  <!-- /.box-body -->

                  <div class="box-footer">
                    <button disabled onclick="myFunctionUpdate()" type="submit" class="btn btn-primary pull-right">Update</button>
                  </div>
                </form>
              </div>
        </div>
    </section>
    <!-- /.content -->
    

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.13
    </div>
    <strong>Copyright &copy; 2019 <a href="#">SteelytoeXyz</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>


<!-- Select2 -->
<script src="../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../bower_components/moment/min/moment.min.js"></script>
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../plugins/iCheck/icheck.min.js"></script>




<!-- DataTables -->
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>



<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : true
    });
  });
  
   $(function() {
        $('#prefix').on('keypress', function(e) {
            if (e.which == 32){
                console.log('Space Detected');
                return false;
            }
        });
        
});

 $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePicker24Hour: true, timePickerIncrement: false, locale: { format: 'YYYY-MM-DD hh:mm:ss' }})
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })
    
    $('#datepickermin').datepicker({
      autoclose: true
    })
    $('#datepickermax').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    });
  });
  

  
function myFunction() {
  var x = document.getElementById("search");
  x.value = x.value.toUpperCase();
}

function myFunctionUpdate() {
  confirm("Tekan Ok jika ingin mengupdate semua kupon!!!");
}
  
  
</script>

</body>
</html>
